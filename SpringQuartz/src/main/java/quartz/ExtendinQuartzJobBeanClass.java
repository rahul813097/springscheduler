package quartz;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;
public class ExtendinQuartzJobBeanClass extends QuartzJobBean {
	private Task task;
	public void setTask(Task task) {
		this.task = task;
	}
	@Override
	protected void executeInternal(JobExecutionContext jec) throws JobExecutionException {
		task.doTask();
	}

}