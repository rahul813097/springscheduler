package quartz;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import static org.quartz.JobBuilder.*;
import static org.quartz.TriggerBuilder.*;

import org.quartz.JobDetail;

import static org.quartz.SimpleScheduleBuilder.*;
public class ScheduleJob {
	
	public static void main(String...s)throws SchedulerException
	{
		SchedulerFactory schedulerFactory=new StdSchedulerFactory();
		Scheduler scheduler=schedulerFactory.getScheduler();
		 // define the job and tie it to our MyTaskclass
		  JobDetail job = newJob(MyTask.class)
		      .withIdentity("j", "g")
		      .build();
		// Trigger the job to run now, and then repeat every 2 seconds
		  Trigger trigger = newTrigger()
		      .withIdentity("trigger1", "group1")
		      .startNow()
		      .withSchedule(simpleSchedule()
		              .withIntervalInSeconds(2)
		              .repeatForever())
		      .build();

		  // Tell quartz to schedule the job using our trigger
		  scheduler.scheduleJob(job, trigger); 
		scheduler.start();
	}
}
