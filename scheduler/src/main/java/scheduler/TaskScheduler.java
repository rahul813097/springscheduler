package scheduler;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
public class TaskScheduler {
public static void main(String ...s)
{
	new AnnotationConfigApplicationContext(Task.class);
	new AnnotationConfigApplicationContext(Work.class);

}
}
