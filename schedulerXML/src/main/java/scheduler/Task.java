package scheduler;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component("myComponent")
public class Task {
	private static final Logger logger=LogManager.getLogger();
	public void doTask()
	{
		logger.info("into do task method of Task class");
		System.out.println("working "+new Date());
	}

}
