package scheduler;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;
public class TaskScheduler {
public static void main(String ...s)
{
	//3 different container for 3 diff spring configuration file
	
	new ClassPathXmlApplicationContext("spring.xml");
	new ClassPathXmlApplicationContext("springPureXml.xml");
	new ClassPathXmlApplicationContext("springAnnotationDriven.xml");
}
}
