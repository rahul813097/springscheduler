package scheduler;

import org.springframework.scheduling.annotation.Scheduled;

public class TaskAnnotationDriven {
	@Scheduled(cron="*/1 * * * * *")
	public void executeTask()
	{
		System.out.println("working with corn and annotation driven");
		
	}
}
