package scheduler;
import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class Task {
	private static final Logger logger=LogManager.getLogger(Task.class);
	public void doTask()
	{
		logger.info("into do task method of Task class");
		System.out.println("working "+new Date());
	}

}
